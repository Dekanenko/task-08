package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.ErrorHandler;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		List<InfoClass> list = new ArrayList<>();
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		validationCheck(xmlFileName);
		Document document = domController.MyDOMParser();
		list = domController.FillList(document);

		//sort  (case 1)
		sortByName(list);
		
		// save
		String outputXmlFile = "output.dom.xml";
		document = createDoc(list);
		XMLSave(document, outputXmlFile);
		validationCheck(outputXmlFile);

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		validationCheck(xmlFileName);
		list = saxController.MySAXParser();

		// PLACE YOUR CODE HERE
		
		// sort  (case 2)
		sortByTemperature(list);
		// save
		outputXmlFile = "output.sax.xml";
		document = createDoc(list);
		XMLSave(document, outputXmlFile);
		validationCheck(outputXmlFile);
		
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		list = staxController.MySTAXParser();
		// PLACE YOUR CODE HERE
		
		// sort  (case 3)
		sortByOrigin(list);
		
		// save
		outputXmlFile = "output.stax.xml";
		document = createDoc(list);
		XMLSave(document, outputXmlFile);
		validationCheck(outputXmlFile);
	}

	public static  void printList(List<InfoClass> list){
		for(InfoClass infoClass : list){
			System.out.println(infoClass);
		}
	}

	public static void sortByName(List<InfoClass> list){
		list.sort((o1, o2) -> o1.getName().compareToIgnoreCase(o2.getName()));
	}

	public static void sortByTemperature(List<InfoClass> list){
		list.sort((o1, o2) -> -o1.getTempreture()+o2.getTempreture());
	}

	public static void sortByOrigin(List<InfoClass> list){
		list.sort((o1, o2) -> o1.getOrigin().compareToIgnoreCase(o2.getOrigin()));
	}

	public static void validationCheck(String fileName){
		Schema schema = null;
		String language = XMLConstants.W3C_XML_SCHEMA_NS_URI;
		SchemaFactory schemaFactory = SchemaFactory.newInstance(language);
		try{
			schema = schemaFactory.newSchema(new File("input.xsd"));
			Validator validator = schema.newValidator();
			ErrorHandler errorHandler = new MyErrorHandler();
			validator.setErrorHandler(errorHandler);
			Source source = new StreamSource(new File(fileName));
			validator.validate(source);
			System.out.println("The file " + fileName + " is valid");
		}catch (Exception ex){
			System.out.println("The file " + fileName + " is NOT valid");
			ex.printStackTrace();
		}
	}

	public static Document createDoc(List<InfoClass> list){
		Document doc = null;
		Element element;
		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
		try{
			DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
			doc = documentBuilder.newDocument();
			Element root = doc.createElement("flowers");
			root.setAttribute("xmlns", "http://www.nure.ua");
			root.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
			root.setAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd 2 ");
			doc.appendChild(root);
			for(InfoClass infoClass : list){
				Element flower = doc.createElement("flower");
				root.appendChild(flower);

				element = doc.createElement("name");
				element.appendChild(doc.createTextNode(infoClass.getName()));
				flower.appendChild(element);

				element = doc.createElement("soil");
				element.appendChild(doc.createTextNode(infoClass.getSoil()));
				flower.appendChild(element);

				element = doc.createElement("origin");
				element.appendChild(doc.createTextNode(infoClass.getOrigin()));
				flower.appendChild(element);

				//visual parameters
				Element visualParameters = doc.createElement("visualParameters");
				flower.appendChild(visualParameters);

				element = doc.createElement("stemColour");
				element.appendChild(doc.createTextNode(infoClass.getStemColour()));
				visualParameters.appendChild(element);

				element = doc.createElement("leafColour");
				element.appendChild(doc.createTextNode(infoClass.getLeafColour()));
				visualParameters.appendChild(element);

				if(infoClass.getAveLenFlowerMeasure()!=null){
					element = doc.createElement("aveLenFlower");
					element.setAttribute("measure", infoClass.getAveLenFlowerMeasure());
					element.appendChild(doc.createTextNode(""+infoClass.getAveLenFlower()));
					visualParameters.appendChild(element);
				}

				//growingTips
				Element growingTips = doc.createElement("growingTips");
				flower.appendChild(growingTips);

				element = doc.createElement("tempreture");
				element.setAttribute("measure", infoClass.getTemperatureMeasure());
				element.appendChild(doc.createTextNode(""+infoClass.getTempreture()));
				growingTips.appendChild(element);

				element = doc.createElement("lighting");
				element.setAttribute("lightRequiring", infoClass.getLightRequiring());
				growingTips.appendChild(element);

				if(infoClass.getWateringMeasure()!=null){
					element = doc.createElement("watering");
					element.setAttribute("measure", infoClass.getWateringMeasure());
					element.appendChild(doc.createTextNode(""+infoClass.getWatering()));
					growingTips.appendChild(element);
				}

				element = doc.createElement("multiplying");
				element.appendChild(doc.createTextNode(infoClass.getMultiplying()));
				flower.appendChild(element);
			}

		}catch (ParserConfigurationException ex){
			ex.printStackTrace();
		}

		return doc;
	}

	public static void XMLSave(Document document, String filename){
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = null;
		try{
			transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
		}catch (TransformerConfigurationException ex){
			ex.printStackTrace();
		}

		DOMSource domSource = new DOMSource(document);
		StreamResult result = new StreamResult(new File(filename));
		try{
			transformer.transform(domSource, result);
		}catch (TransformerException ex){
			ex.printStackTrace();
		}
		System.out.println("File: "+filename+" saved!");
	}


}
