package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.InfoClass;
import com.epam.rd.java.basic.task8.MyErrorHandler;
import com.epam.rd.java.basic.task8.MyHandler;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.swing.text.Document;
import javax.xml.XMLConstants;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	
	private String xmlFileName;
	public List<InfoClass> list = new ArrayList<>();

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public List<InfoClass> MySAXParser(){
		Schema schema = null;
		String language = XMLConstants.W3C_XML_SCHEMA_NS_URI;
		SchemaFactory schemaFactory = SchemaFactory.newInstance(language);
		try{
			schema = schemaFactory.newSchema(new File("input.xsd"));
		} catch (SAXException e) {
			e.printStackTrace();
		}

		SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
		try{
			SAXParser saxParser = saxParserFactory.newSAXParser();
			DefaultHandler myHandler = new MyHandler(list);
			saxParser.parse(xmlFileName, myHandler);
		}catch (ParserConfigurationException | SAXException | FileNotFoundException ex){
			ex.printStackTrace();
		}catch (IOException ex){
			ex.printStackTrace();
		}
		return list;
	}

}