package com.epam.rd.java.basic.task8;

public class InfoClass {
    private String name;
    private String soil;
    private String origin;
    //visual parameters
    private String stemColour;
    private String leafColour;
    private int aveLenFlower;
    private String aveLenFlowerMeasure;
    //growingTips
    private int tempreture;
    private String temperatureMeasure;
    private String lightRequiring;
    private int watering;
    private String wateringMeasure;

    private String multiplying;

    public void setName(String name) {
        this.name = name;
    }

    public void setSoil(String soil) {
        this.soil = soil;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public void setStemColour(String stemColour) {
        this.stemColour = stemColour;
    }

    public void setLeafColour(String leafColour) {
        this.leafColour = leafColour;
    }

    public void setAveLenFlower(int aveLenFlower) {
        this.aveLenFlower = aveLenFlower;
    }

    public void setAveLenFlowerMeasure(String aveLenFlowerMeasure) {
        this.aveLenFlowerMeasure = aveLenFlowerMeasure;
    }

    public void setTempreture(int tempreture) {
        this.tempreture = tempreture;
    }

    public void setTempretureMeasure(String temperatureMeasure) {
        this.temperatureMeasure = temperatureMeasure;
    }

    public void setLightRequiring(String lightRequiring) {
        this.lightRequiring = lightRequiring;
    }

    public void setWatering(int watering) {
        this.watering = watering;
    }

    public void setWateringMeasure(String wateringMeasure) {
        this.wateringMeasure = wateringMeasure;
    }

    public void setMultiplying(String multiplying) {
        this.multiplying = multiplying;
    }

    public String getName() {
        return name;
    }

    public String getSoil() {
        return soil;
    }

    public String getOrigin() {
        return origin;
    }

    public String getStemColour() {
        return stemColour;
    }

    public String getLeafColour() {
        return leafColour;
    }

    public int getAveLenFlower() {
        return aveLenFlower;
    }

    public String getAveLenFlowerMeasure() {
        return aveLenFlowerMeasure;
    }

    public int getTempreture() {
        return tempreture;
    }

    public String getTemperatureMeasure() {
        return temperatureMeasure;
    }

    public String getLightRequiring() {
        return lightRequiring;
    }

    public int getWatering() {
        return watering;
    }

    public String getWateringMeasure() {
        return wateringMeasure;
    }

    public String getMultiplying() {
        return multiplying;
    }

    @Override
    public String toString() {
        return "InfoClass{" +
                "name='" + name + '\'' +
                ", soil='" + soil + '\'' +
                ", origin='" + origin + '\'' +
                ", stemColour='" + stemColour + '\'' +
                ", leafColour='" + leafColour + '\'' +
                ", aveLenFlower='" + aveLenFlower + '\'' +
                ", aveLenFlowerMeasure='" + aveLenFlowerMeasure + '\'' +
                ", temperature=" + tempreture +
                ", temperatureMeasure='" + temperatureMeasure + '\'' +
                ", lightRequiring='" + lightRequiring + '\'' +
                ", watering=" + watering +
                ", wateringMeasure='" + wateringMeasure + '\'' +
                ", multiplying='" + multiplying + '\'' +
                '}';
    }
}
