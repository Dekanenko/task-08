package com.epam.rd.java.basic.task8;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class MyHandler extends DefaultHandler {

    private List<InfoClass> list = new ArrayList<>();
    private String localName;
    private String attrValue = "";

    public MyHandler(List<InfoClass> list) {
        this.list = list;
    }

    @Override
    public void startDocument() throws SAXException {
        System.out.println("Start Document Processing");
    }

    @Override
    public void endDocument() throws SAXException {
        System.out.println("End Document Processing");
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        this.localName = qName;
        if(qName.equals("flower"))
            list.add(new InfoClass());
        if(attributes.getLength()>0&&!qName.equals("flowers")){
            attrValue = attributes.getValue(0).trim();
            if(qName.equals("lighting")){
                characters(new char[]{'0','0'},0,2);
            }
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        attrValue = "";
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        String str = new String(ch, start, length).trim();

        if(localName.equals("flowers")||localName.equals("flower")
                ||localName.equals("visualParameters")||localName.equals("growingTips")||str.length()<1)
            return;

        char[] c = localName.toCharArray();
        Method method = null;

        c[0] = Character.toUpperCase(c[0]);
        String mStr = new String(c);
        mStr = "set"+mStr;

        Class cls = list.get(list.size()-1).getClass();

        try {
            if(this.attrValue.equals("yes")||this.attrValue.equals("no")){
                method = cls.getDeclaredMethod("setLightRequiring", String.class);
                method.invoke(list.get(list.size()-1), attrValue);
                return;
            }
            if(localName.equals("aveLenFlower")||localName.equals("watering")
                    ||localName.equals("tempreture")){
                method = cls.getDeclaredMethod(mStr, int.class);
                method.invoke(list.get(list.size()-1), Integer.parseInt(str));
                mStr+="Measure";
                method = cls.getDeclaredMethod(mStr, String.class);
                method.invoke(list.get(list.size()-1), attrValue);
            }else {
                method = cls.getDeclaredMethod(mStr, String.class);
                method.invoke(list.get(list.size()-1), str);
            }

        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}
