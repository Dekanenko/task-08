package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.InfoClass;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private String xmlFileName;
	private List<InfoClass> list = new ArrayList<>();

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public List<InfoClass> MySTAXParser(){
		XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
		Method method = null;
		Class cls = null;

		try {
			XMLEventReader reader = xmlInputFactory.createXMLEventReader(new FileInputStream(xmlFileName));
			while (reader.hasNext()){
				XMLEvent event = reader.nextEvent();
				if(event.isStartElement()){
					StartElement startElement = event.asStartElement();
					if(!event.asStartElement().getName().getLocalPart().equals("flowers")
							&&!event.asStartElement().getName().getLocalPart().equals("visualParameters")
							&&!event.asStartElement().getName().getLocalPart().equals("growingTips"))
					{
						if(event.asStartElement().getName().getLocalPart().equals("flower"))
							list.add(new InfoClass());
						else
						{
							event = reader.nextEvent();
							if(event.isCharacters()){
								char[] c = startElement.getName().getLocalPart().toCharArray();

								c[0] = Character.toUpperCase(c[0]);
								String mStr = new String(c);
								mStr = "set"+mStr;

								cls = list.get(list.size()-1).getClass();

									if(startElement.getName().getLocalPart().equals("aveLenFlower")
											||startElement.getName().getLocalPart().equals("watering")
											||startElement.getName().getLocalPart().equals("tempreture"))
									{
										method = cls.getDeclaredMethod(mStr, int.class);
										method.invoke(list.get(list.size()-1), Integer.parseInt(event.asCharacters().getData()));
										mStr+="Measure";
										method = cls.getDeclaredMethod(mStr, String.class);
										method.invoke(list.get(list.size()-1), startElement.getAttributes().next().getValue());
									}else
									{
										method = cls.getDeclaredMethod(mStr, String.class);
										method.invoke(list.get(list.size()-1), event.asCharacters().getData());
									}

							}
						}
						if(startElement.getName().getLocalPart().equals("lighting")){
							method = cls.getDeclaredMethod("setLightRequiring", String.class);
							method.invoke(list.get(list.size()-1), startElement.getAttributes().next().getValue());
						}
					}
				}
			}
		}catch (FileNotFoundException | XMLStreamException | NoSuchMethodException | IllegalAccessException | InvocationTargetException ex){
			ex.printStackTrace();
		}
		return list;
	}

}