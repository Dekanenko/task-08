package com.epam.rd.java.basic.task8;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class MyErrorHandler implements ErrorHandler {

    public void error(SAXParseException ex) throws SAXException {
        System.err.println("Error:   " + ex);
        System.err.println("\tline = " + ex.getLineNumber() + ", col = "
                + ex.getColumnNumber());
        throw ex;
    }

    public void fatalError(SAXParseException ex) throws SAXException {
        System.err.println("Fatal Error:   " + ex);
        System.err.println("\tline = " + ex.getLineNumber() + ", col = "
                + ex.getColumnNumber());
        System.exit(-1);
    }

    public void warning(SAXParseException ex) throws SAXException {
        System.err.println("Warning:   " + ex);
        System.err.println("\tline = " + ex.getLineNumber() + "    col = "
                + ex.getColumnNumber());
    }

}
