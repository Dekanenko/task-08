package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.InfoClass;
import com.epam.rd.java.basic.task8.MyErrorHandler;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private String xmlFileName;
	public List<InfoClass> list = new ArrayList<>();

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public Document MyDOMParser(){
		Schema schema = null;
		String language = XMLConstants.W3C_XML_SCHEMA_NS_URI;
		SchemaFactory schemaFactory = SchemaFactory.newInstance(language);
		try{
			schema = schemaFactory.newSchema(new File("input.xsd"));
		} catch (SAXException e) {
			e.printStackTrace();
		}
		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
		documentBuilderFactory.setNamespaceAware(true);
		documentBuilderFactory.setSchema(schema);
		documentBuilderFactory.setIgnoringElementContentWhitespace(true);
		File file = new File(xmlFileName);
		Document document = null;
		try{
			DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
			document = documentBuilder.parse(file);
		}catch (ParserConfigurationException | SAXException | IOException ex){
			ex.printStackTrace();
		}
		return document;
	}

	public List<InfoClass> FillList(Document document){
		NodeList nodeList = document.getElementsByTagName("flower");
		NamedNodeMap namedNodeMap;
		for(int i = 0; i<nodeList.getLength();i++){
			list.add(new InfoClass());
			list.get(i).setName(document.getElementsByTagName("name").item(i).getTextContent());
			list.get(i).setSoil(document.getElementsByTagName("soil").item(i).getTextContent());
			list.get(i).setOrigin(document.getElementsByTagName("origin").item(i).getTextContent());

			NodeList innerList = document.getElementsByTagName("visualParameters");
			list.get(i).setStemColour(innerList.item(i).getChildNodes().item(0).getTextContent());
			list.get(i).setLeafColour(innerList.item(i).getChildNodes().item(1).getTextContent());
			if(innerList.item(i).getChildNodes().getLength()>2){
				list.get(i).setAveLenFlower(Integer.parseInt(innerList.item(i).getChildNodes().item(2).getTextContent()));
				namedNodeMap = innerList.item(i).getChildNodes().item(2).getAttributes();
				list.get(i).setAveLenFlowerMeasure(namedNodeMap.item(0).getTextContent());
			}

			innerList = document.getElementsByTagName("growingTips");
			list.get(i).setTempreture(Integer.parseInt(innerList.item(i).getChildNodes().item(0).getTextContent()));

			namedNodeMap = innerList.item(i).getChildNodes().item(0).getAttributes();
			list.get(i).setTempretureMeasure(namedNodeMap.item(0).getTextContent());

			namedNodeMap = innerList.item(i).getChildNodes().item(1).getAttributes();
			list.get(i).setLightRequiring(namedNodeMap.item(0).getTextContent());

			if(innerList.item(i).getChildNodes().getLength()>2){
				list.get(i).setWatering(Integer.parseInt(innerList.item(i).getChildNodes().item(2).getTextContent()));
				namedNodeMap = innerList.item(i).getChildNodes().item(2).getAttributes();
				list.get(i).setWateringMeasure(namedNodeMap.item(0).getTextContent());
			}

			list.get(i).setMultiplying(document.getElementsByTagName("multiplying").item(i).getTextContent());
		}

		return list;
	}



}
